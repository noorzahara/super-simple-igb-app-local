Super Simple IGB App demonstrates how an App can add a new menu item to IGB.

To run the Super Simple IGB App:

1. Install the App.
2. Select **Super Simple IGB App** under **Tools** menu. 
3. Observe that a message dialog window appears on the screen.
