# About

This App demonstrates how to use the IGB API to add a menu item and connect an App to IGB.

This repository shows you how you can use Bitbucket Piplines to build your App **and** use a Bitbucket repository's **Downloads** folder as an App repository for IGB. 

# Bitbucket piplines example

Take a look at the `bitbucket_pipelines.yml` in this repository. Notice that it builds the App using maven and then copies the built artifacts to the repository's Downloads folder.

If you are using Bitbucket, you can do the same. Just enable Pipelines on your repository and add a similar `bitbucket_pipelines.yml`. Then, whenever you push to your repository, 
the pipeline will run, compile your App, make an OBR index file, and copy both artifacts to your Downloads directory.

# How to run this IGB App

There are two ways to try it out!

* * *

## Option 1: Build it on your local computer.

* Clone the repository
* Build the App by running `mvn package`
* Start IGB 
* Select **Open App Manager** from the IGB **Tools** menu
* Click **Manage Repositories** button
* Use the file chooser to select the "target" directory in the cloned repository as a new App repository

Re-open the IGB App Manager. You should now see a new App named "Super Simple IGB App" on the list of available Apps.

Install it and look for a new menu item under the **Tools** menu in IGB.

## Option 2: Use this repository's Downloads folder as an IGB App repository

Because this repository's **Downloads** folder contains an OBR index file (repository.xml), you can try out Super Simple IGB App from there.

To do this, follow the instructions above, but instead of selecting a local folder, enter this URL:

* https://bitbucket.org/lorainelab/super-simple-igb-app/downloads/

Also, if you want to develop a new App of your own, you can use Bitbucket Pipelines to automate building and sharing your App. Take a look at the bitbucket_pipelines.yml and POM.xml files for a guide on how to do this.
